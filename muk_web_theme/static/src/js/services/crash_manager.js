odoo.define('muk_web_theme.CrashManager', function (require) {
    "use strict";
    var CrashManager = require('web.CrashManager');
    var MukThemeCrashManager = CrashManager.include({
        show_error: function(error) {
            if (error) {
                var error_debug = error.data.debug;
                var error_msg = error.message;
                var res = odoo.debranding_new_website;
                if (error.title) {
                     var error_title = error.title;
                     error.title = error_title.replace(/odoo/gi, res);
                }
                if (error_msg)
                    error.message = error_msg.replace(/odoo/gi, res);
                if (error_debug)
                    error.data.debug = error_debug.replace(/odoo/gi, res);
            }
            this._super.apply(this, arguments);
        },

         // In case of issues with warning can be used.
        // show_warning: function(error) {
        //     if (error) {
        //         var error_debug = error.data.debug;
        //         var error_msg = error.message;
        //         var error_title = error.title;
        //         var res = odoo.debranding_new_website;
        //         error.title = error_title.replace(/odoo/gi, res);
        //         error.message = error_msg.replace(/odoo/gi, res);
        //     }
        //     this._super.apply(this, arguments);
        // },

        // In case of issues with message can be used.
         /**
          * @override
          */
        // show_message: function(exception) {
        //     var res = odoo.debranding_new_name;
        //     var exception = exception.replace(/odoo/gi, res);
        //     return this.show_error({
        //         type: _t("System Client Error"),
        //         message: exception,
        //         data: {debug: ""}
        //     });
        // }
    });
    return MukThemeCrashManager;
});