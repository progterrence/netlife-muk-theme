odoo.define('muk_web_theme.MailBotService', function (require) {
    "use strict";
    var mail_bot_service =  require('mail_bot.MailBotService');
    var core = require('web.core');

    var _t = core._t;

    var MailBotService  = mail_bot_service.include({
        /**
         * * @override
         * */
        getPreviews: function (filter) {
            var previews = [];
            if (this.hasRequest() && (filter === 'mailbox_inbox' || !filter)) {
                previews.push({
                    title: _t("NBot has a request"),
                    imageSRC: "muk_web_theme/static/src/img/chat-bot.png",
                    status: 'bot',
                    body:  _t("Enable desktop notifications to chat"),
                    id: 'request_notification',
                    unreadCounter: 1,
                });
            }
        return previews;
    },

    });
    return MailBotService;
});