odoo.define('muk_web_theme.model.MailFailure', function (require) {
    "use strict";

    var mail_failure = require('mail.model.MailFailure');

    var MailFailure = mail_failure.include({
        getPreview: function () {
            if (this._moduleIcon === '/mail/static/src/img/smiley/mailfailure.jpg')
                this._moduleIcon = '/muk_web_theme/static/src/img/error-email.png';
            var preview = {
                body: _t("An error occured when sending an email"),
                date: this._lastMessageDate,
                documentID: this._documentID,
                documentModel: this._documentModel,
                id: 'mail_failure',
                imageSRC: this._moduleIcon,
                title: this._modelName,
            };
            return preview;
            },
    });
});