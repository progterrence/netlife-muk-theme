odoo.define('muk_web_theme.ListView', function (require) {
"use strict";

var dom = require('web.dom');
var core = require('web.core');
var config = require("web.config");
var ListView = require('web.ListView');
var _t = core._t;
var QWeb = core.qweb;

ListView.include({
    mobile_friendly: true

});

});
