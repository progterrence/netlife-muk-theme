odoo.define('muk_web_theme.Menu', function (require) {
"use strict";

var core = require('web.core');
var config = require("web.config");

var Menu = require("web.Menu");
var AppsBar = require("muk_web_theme.AppsBar");

var _t = core._t;
var QWeb = core.qweb;

Menu.include({
    events: _.extend({}, Menu.prototype.events, {
        "click #sidebar-collapse-button": "_onCollapseSidebar",
    	"click .o_menu_apps a[data-toggle=dropdown]": "_onAppsMenuClick",
    	"click .ul_menu_secondary > li > a": "_onItemMenuClick",
    	"click .mk_apps_sidebar_menu > li > a": "_onItemMenuClick",
    	"click .ul_menu_secondary_submenu > li > a": "_onItemMenuClick",
    	"click .mk_menu_mobile_section": "_onMobileSectionClick",
        "click .o_menu_sections [role=menuitem]": "_hideMobileSubmenus",
        "show.bs.dropdown .o_menu_systray, .o_menu_apps": "_hideMobileSubmenus",
    }),
    menusTemplate: config.device.isMobile ? 
    		'muk_web_theme.MobileMenu.sections' : Menu.prototype.menusTemplate,
    start: function () {
    	var res = this._super.apply(this, arguments);
        this.$menu_toggle = this.$(".mk_menu_sections_toggle");
        this.$menu_apps_sidebar = this.$('.mk_apps_sidebar_panel');
        this._appsBar = new AppsBar(this, this.menu_data);
        this._appsBar.appendTo(this.$menu_apps_sidebar);
        this.$menu_apps_sidebar.renderScrollBar();
        var session = this.getSession();
        var $client_company_logo = $('.oe_client_company_logo');
        var $home_client_company_logo = $('.home_oe_client_company_logo');
        var client_company_logo_src = session.url('/web/image', {
            model:'res.company',
            field: 'logo',
            id: session.company_id,
        });
        $client_company_logo.attr('src', client_company_logo_src);
        $home_client_company_logo.attr('src', client_company_logo_src);

        if (config.device.isMobile) {
            var menu_ids = _.keys(this.$menu_sections);
            for (var i = 0; i < menu_ids.length; i++) {
            	var $section = this.$menu_sections[menu_ids[i]];
            	$section.on('click', 'a[data-menu]', this, function(ev) {
                	ev.stopPropagation();
                });
            }
        } 
        return res;
    },
    change_menu_section: function (primary_menu_id) {
        // Patch for hiding sub menu
        var menu = this.menu_data.children.find(
            (x)=> x.id == primary_menu_id && x.children.length !=0) || false;
        var container = this.$menu_toggle;;
        if (container.length != 0) {
                if (!menu && container.hasClass('d-md-none')) {
                    container.removeClass('d-md-none').addClass('d-none');
                }
                if (menu && container.hasClass('d-none')) {
                     container.removeClass('d-none').addClass('d-md-none');
                }
            }
        return this._super.apply(this, arguments);
     },
    _onCollapseSidebar: function (){
        $(body).toggleClass('mk_sidebar_type_large');
        $(body).toggleClass('mk_sidebar_type_small');
        var icon = $('#sidebar-collapse-button').find('i');
        icon.toggleClass('fa-bars');
    },
    _hideMobileSubmenus: function () {
        if (this.$menu_toggle.is(":visible") && $('.oe_wait').length === 0 && 
        		this.$section_placeholder.is(":visible")) {
            this.$section_placeholder.collapse("hide");
        }
    },
    _updateMenuBrand: function () {
        if (!config.device.isMobile) {
            return this._super.apply(this, arguments);
        }
    },
    _onAppsMenuClick: function(event, checkedCanBeRemoved) {
    	var action_manager = this.getParent().action_manager;
    	var controller = action_manager.getCurrentController();
    	if (controller && !checkedCanBeRemoved) {
    		controller.widget.canBeRemoved().done(function () {
    			$(event.currentTarget).trigger('click', [true]);
    			$(event.currentTarget).off('.bs.dropdown');
            });
        	event.stopPropagation();
        	event.preventDefault();
        }
    },
    _onItemMenuClick: function (event) {
        $('.active-menu').removeClass('active-menu');
        var $menu = $(event.currentTarget);
        $menu.toggleClass('active-menu');
    },
    _onMobileSectionClick: function (event) {
    	event.preventDefault();
    	event.stopPropagation();
    	var $section = $(event.currentTarget);
    	if ($section.hasClass('show')) {
    		$section.removeClass('show');
    		$section.find('.show').removeClass('show');
    		$section.find('.fa-chevron-down').hide();
    		$section.find('.fa-chevron-right').show();
    	} else {
    		$section.addClass('show');
    		$section.find('ul:first').addClass('show');
    		$section.find('.fa-chevron-down:first').show();
    		$section.find('.fa-chevron-right:first').hide();
    	}
    },
});

});