/*  Copyright 2015-2018 Ivan Yelizariev <https://it-projects.info/team/yelizariev>
    Copyright 2017 ArtyomLosev <https://github.com/ArtyomLosev>
    License MIT (https://opensource.org/licenses/MIT). */
odoo.define("muk_web_theme.base", function(require) {
    "use strict";
    var WebClient = require("web.WebClient");

    WebClient.include({
        init: function(parent) {
            this._super.apply(this, arguments);
            var self = this;
            odoo.debranding_new_name = "";
            odoo.debranding_new_website = "";
            self._rpc({
                model: "ir.config_parameter",
                method: "get_branding_param",
                args: ['muk_branding.system_name'],
            }).then(function(result) {
                odoo.debranding_new_name = result;
                odoo.debranding_new_website = result;
            });
        },
    });
});
